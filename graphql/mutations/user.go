package mutations

import (
	"server/repository"
	"server/types"
	"server/utils"
	"server/security"
	log "github.com/sirupsen/logrus"
	"errors"
	"github.com/graphql-go/graphql"
)

type DataSignIn struct {
	User		types.User
	Token		string
}

// DataSignInType is an output GraphQL schema for the project user block relation type.
var DataSignInType = graphql.NewObject(graphql.ObjectConfig{
	Name: "DataSignIn",
	Fields: graphql.Fields{
		"user":   &graphql.Field{Type: types.UserType},
		"token":   &graphql.Field{Type: graphql.String},
	},
})

// CreateUser creates a new user and returns it.
func CreateUser() *graphql.Field {
	return &graphql.Field{
		Type: DataSignInType,
		Args: graphql.FieldConfigArgument{
			"firstname": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"username": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"lastname": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"password": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"email": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[mutation] create user\n")
			log.WithFields(log.Fields{
				"firstname": params.Args["firstname"].(string),
				"lastname":  params.Args["lastname"].(string),
				"password":  params.Args["password"].(string),
				"username":  params.Args["username"].(string),
				"email":  params.Args["email"].(string),
			}).Debug("__ create user\n")


			hashedPassword, err := utils.HashPassword(params.Args["password"].(string))
			if err != nil {
				log.Debug(err)
			}
			user := &types.User{
				Username: params.Args["username"].(string),
				Firstname: params.Args["firstname"].(string),
				Lastname:  params.Args["lastname"].(string),
				Email:     params.Args["email"].(string),
				Password:  hashedPassword,
				CreatedAt: utils.GetDate(),
			}

			repository.Create(user)

			token , err := security.GenerateToken(user.Username)
			if err != nil {
				data := &DataSignIn{User :  *user}
				return data, errors.New("Token cannot be generated")
			}
			data := &DataSignIn{
				User : *user,
				Token : token,
			}
			return data, nil
		},
	}
}

