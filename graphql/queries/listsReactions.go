package queries

import (
	log "github.com/sirupsen/logrus"

	"server/repository"
	"server/types"

	"github.com/satori/go.uuid"
	"github.com/graphql-go/graphql"
)

// FindListReaction return list of reactions from a project
func FindListReaction() *graphql.Field {
	return &graphql.Field{
		Type:        graphql.NewList(types.ReactionType),
		Description: "return list of reactions",
		Args: graphql.FieldConfigArgument{
			"id_project": &graphql.ArgumentConfig{
				Type:         graphql.NewNonNull(graphql.String),
			},
			"offset": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: -1,
			},
			"limit": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: -1,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] FindListReactionBy\n")
			log.WithFields(log.Fields{
				"id":     params.Args["id_project"].(string),
				"offset": params.Args["offset"].(int),
				"limit":  params.Args["limit"].(int),
			}).Debug("__ Query ListsReactionBy\n")

			var ret []types.ListsReactions
			var reaction []types.Reaction

			id, _ := uuid.FromString(params.Args["id_project"].(string))

			repository.FindListBy(&ret, "id_project", id, params.Args["offset"].(int), params.Args["limit"].(int))

			listIndex := getIdReaction(ret)

			repository.FindByList(&reaction, listIndex)

			return reaction, nil
		},
	}
}

func getIdReaction(source []types.ListsReactions) ([]string) {
	new_tab := make([]string, len(source))

	for i, e := range source {
		new_tab[i] = e.ID_reaction.String()
	}

	return new_tab
}
