package queries

import (
	"server/repository"
	"server/types"
	"server/utils"
	"server/security"

	"errors"
	"fmt"

	log "github.com/sirupsen/logrus"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/graphql-go/graphql"
)

type DataLogin struct {
	User		types.User
	Token		string
}

// DataLoginType is an output GraphQL schema for the project user block relation type.
var DataLoginType = graphql.NewObject(graphql.ObjectConfig{
	Name: "DataLogin",
	Fields: graphql.Fields{
		"user":   &graphql.Field{Type: types.UserType},
		"token":   &graphql.Field{Type: graphql.String},
	},
})

func Login() *graphql.Field {
	return &graphql.Field{
		Type:        DataLoginType,
		Description: "Return User from Email",
		Args: graphql.FieldConfigArgument{
			"username": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"password": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] Login \n")
			log.WithFields(log.Fields{
				"username": params.Args["username"].(string),
				"password": params.Args["password"].(string),
			}).Debug("__ Query Login\n")

			var user types.User

			ret := repository.FindUser(&user, params.Args["username"].(string))
			if ret {
				data := &DataLogin{}
				return data, errors.New("No user find")
			}

			if utils.CheckPasswordHash(params.Args["password"].(string), user.Password) {
				token , err := security.GenerateToken(user.Username)
				if err != nil {
					data := &DataLogin{}
					return data, errors.New("Token cannot be generated")
				} else {
					data := &DataLogin{
						User : user,
						Token : token,
					}
					return data, nil
				}
			} else {
				data := &DataLogin{}
				return data, errors.New("password invalid")
			}

		},
	}
}

func Refresh() *graphql.Field {
	return &graphql.Field{
		Type:        DataLoginType,
		Description: "Refresh Token",
		Args: graphql.FieldConfigArgument{
			"token": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] Refresh \n")

			token, err := jwt.Parse(params.Args["token"].(string), security.CheckWithSecretKey)

			if err != nil {
				return &DataLogin{}, fmt.Errorf("access denied")
			}
			claims, ok := token.Claims.(jwt.MapClaims)

			if !(ok && token.Valid) {
				return &DataLogin{}, fmt.Errorf("access denied")
			}

			new_token, errr := security.GenerateToken(claims["username"].(string))
			if errr != nil {
				return &DataLogin{}, errr
			}
			data := &DataLogin{
				Token : new_token,
			}
			return data, nil

		},
	}
}