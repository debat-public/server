package queries

import (
	log "github.com/sirupsen/logrus"

	"server/repository"
	"server/types"

	"github.com/satori/go.uuid"
	"github.com/graphql-go/graphql"
)

// FindListUser return list of user from a role
func FindListUser() *graphql.Field {
	return &graphql.Field{
		Type:        graphql.NewList(types.ListUsersType),
		Description: "return list of user by role",
		Args: graphql.FieldConfigArgument{
			"id_role": &graphql.ArgumentConfig{
				Type:         graphql.NewNonNull(graphql.String),
			},
			"offset": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: -1,
			},
			"limit": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: -1,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] FindListUsersBy\n")
			log.WithFields(log.Fields{
				"id":     params.Args["id_role"].(string),
				"offset": params.Args["offset"].(int),
				"limit":  params.Args["limit"].(int),
			}).Debug("__ Query ListUsersBy\n")

			var ListUsers []types.ListUsers

			id, _ := uuid.FromString(params.Args["id_role"].(string))

			repository.FindListBy(&ListUsers, "id_role", id, params.Args["offset"].(int), params.Args["limit"].(int))

			return ListUsers, nil
		},
	}
}
