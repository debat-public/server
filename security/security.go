package security

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"net/http"
	"server/config"

	jwt "github.com/dgrijalva/jwt-go"
)

// checkWithSecretKey check the JWT
func CheckWithSecretKey(token *jwt.Token) (interface{}, error) {
	_, ok := token.Method.(*jwt.SigningMethodHMAC)
	if !ok {
		return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
	}
	config := config.GetConfig()
	return []byte(config.JWT.Secret), nil
}

// Handle security middleware aims to implement a JWT authentication.
func Handle(next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")

		header := r.Header.Get("Authorization")

		if header == "" {
			next.ServeHTTP(w, r)
			return
		}

		tokenString := header

		token, err := jwt.Parse(tokenString, CheckWithSecretKey)

		if err != nil {
			log.WithFields(log.Fields{
				"err":     err,
			}).Debug("__ err\n")
			http.Error(w, "JWT Authenticated failed", http.StatusUnauthorized)
			return
		}

		claims, ok := token.Claims.(jwt.MapClaims)

		if ok && token.Valid {
			log.Info("JWT Authenticated OK (config: %s)", claims["exp"])

			next.ServeHTTP(w, r)
		}
	})
}
