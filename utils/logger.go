package utils

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
)

// InitLogger initialize the Logger
func InitLogger(level string) {
	var err error

	fmt.Println("Init logger ...")

	file, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		fmt.Println(err)
	}

	log.SetOutput(file)

	if level == "debug" {
		log.Info("Start log in DEBUG MOD")
		log.SetLevel(log.DebugLevel)
	} else if level == "error" {
		log.Info("Start log in ERROR MOD")
		log.SetLevel(log.ErrorLevel)
	} else {
		log.Info("Start log in INFO MOD")
		log.SetLevel(log.InfoLevel)
	}

}
