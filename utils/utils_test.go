package utils

import (
	"encoding/json"
	"testing"
)

type Struttest struct {
	Nom    string `json:"nom"`
	Prenom string `json:"prenom"`
}

func doReadJSON(file string) (Struttest, bool, error) {
	var structTest Struttest
	var err error
	byteJSON, err := ReadJSON(file)
	if err != nil {
		return structTest, false, err
	} else {
		err = json.Unmarshal(byteJSON, &structTest)
		if err == nil {
			return structTest, true, err
		} else {
			return structTest, false, err
		}
	}
}

func compStruct(s1 Struttest, s2 Struttest) bool {
	return s1.Nom == s2.Nom && s1.Prenom == s2.Prenom
}

func TestJson(t *testing.T) {
	var structTest Struttest
	var err error
	var stat bool

	var flagTest = []struct {
		input  string
		output Struttest
		stat   bool
	}{
		{"", Struttest{"", ""}, false},
		{"/json_test.json", Struttest{"mokhtari", "sofiane"}, true},
		{"/json_test.json", Struttest{"mokhtari", "sofiane"}, true},
	}

	for i, tt := range flagTest {
		structTest, stat, err = doReadJSON(tt.input)
		if tt.stat != stat {
			t.Log(err)
			t.Error("Test numero ", i, " FAIL 1")
		} else if !compStruct(tt.output, structTest) {
			t.Log(tt.output)
			t.Log(structTest)
			t.Error("Test numero ", i, " FAIL 2")
		}
	}
}
