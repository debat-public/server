package utils

import (
	"time"
	"strconv"
)

func GetDate() (string) {
	date := time.Now()
	return date.String()
}

// TimestampToAge return the age of the user
func TimestampToAge(input string) string {

	now := time.Now()
	age, _ := time.Parse(time.RFC3339, input)

	y1, M1, d1 := age.Date()
	y2, M2, d2 := now.Date()

	h1, m1, s1 := age.Clock()
	h2, m2, s2 := now.Clock()

	year := int(y2 - y1)
	month := int(M2 - M1)
	day := int(d2 - d1)
	hour := int(h2 - h1)
	min := int(m2 - m1)
	sec := int(s2 - s1)

	// Normalize negative values
	if sec < 0 {
		sec += 60
		min--
	}
	if min < 0 {
		min += 60
		hour--
	}
	if hour < 0 {
		hour += 24
		day--
	}
	if day < 0 {
		// days in month:
		t := time.Date(y1, M1, 32, 0, 0, 0, 0, time.UTC)
		day += 32 - t.Day()
		month--
	}
	if month < 0 {
		month += 12
		year--
	}

	return strconv.Itoa(year)
}
