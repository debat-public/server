package types

import (
	"github.com/graphql-go/graphql"
	"github.com/satori/go.uuid"
	"github.com/jinzhu/gorm"
)

// Role type definition.
type Block struct {
	ID    uuid.UUID	`gorm:"type:uuid;primary_key;" db:"id" json:"id"`
	Title string 	`db:"title" json:"title"`
	Body  string 	`db:"body" json:"body"`
	Index  int 		`db:"index" json:"index"`
}

// BlockType is the GraphQL schema for the user type.
var BlockType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Block",
	Fields: graphql.Fields{
		"id":    &graphql.Field{Type: graphql.String},
		"title": &graphql.Field{Type: graphql.String},
		"body":  &graphql.Field{Type: graphql.String},
		"index": &graphql.Field{Type: graphql.Int},
	},
})

// BeforeCreate will set a UUID rather than numeric ID.
func (block *Block) BeforeCreate(scope *gorm.Scope) error {
	uuid := uuid.NewV4()
	return scope.SetColumn("ID", uuid)
}

var	BlockInputType =  graphql.NewInputObject(graphql.InputObjectConfig{
		Name: "BlockInput",
		Fields: graphql.InputObjectConfigFieldMap{
			"title": &graphql.InputObjectFieldConfig{Type: graphql.String},
			"body":  &graphql.InputObjectFieldConfig{Type: graphql.String},
			"index": &graphql.InputObjectFieldConfig{Type: graphql.Int},
		},
	})
	