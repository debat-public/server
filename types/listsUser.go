package types

import (
	"github.com/graphql-go/graphql"
	"github.com/satori/go.uuid"
)

// Lists Opinions type definition.
type ListUsers struct {
	ID_user uuid.UUID    `gorm:"type:uuid" db:"id_user" json:"id_user"`
	ID_role uuid.UUID    `gorm:"type:uuid" db:"id_role" json:"id_role"`
}

// ListUsersType is the GraphQL schema for the role user relation type.
var ListUsersType = graphql.NewObject(graphql.ObjectConfig{
	Name: "ListUsers",
	Fields: graphql.Fields{
		"id_user": &graphql.Field{Type: graphql.String},
		"id_role": &graphql.Field{Type: graphql.String},
	},
})
