package types

import (
	"github.com/graphql-go/graphql"
	"github.com/satori/go.uuid"
)

// Lists Reactions type definition.
type ListsReactions struct {
	ID_reaction	uuid.UUID	`gorm:"type:uuid" db:"id_reaction" json:"id_reaction"`
	ID_user	uuid.UUID	`gorm:"type:uuid" db:"id_user" json:"id_user"`
	ID_project	uuid.UUID	`gorm:"type:uuid" db:"id_project" json:"id_project"`
}

// ListsReactionsType is the GraphQL schema for the lists reactions type.
var ListsReactionsType = graphql.NewObject(graphql.ObjectConfig{
	Name: "ListsReactions",
	Fields: graphql.Fields{
		"id_reaction": &graphql.Field{Type: graphql.String},
		"id_user":   &graphql.Field{Type: graphql.String},
		"id_project":  &graphql.Field{Type: graphql.String},
	},
})
