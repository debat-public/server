package repository

import (
	"github.com/satori/go.uuid"
)

type repository interface {
	Find(interface{}, uuid.UUID) error
	FindUser(interface{}, string) bool
	FindByList(interface{}, []string) error
	FindAll(interface{}, int, int) error
	FindListBy(interface{}, string, uuid.UUID, int) error
	FindListSelectedBy(interface{},string, string, uuid.UUID, int) error
	Create(interface{}) error
}
